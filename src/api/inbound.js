import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/inbound',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/inbound/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/inbound',
    method: 'put',
    data
  })
}

export function downloadInbound(params) {
  return request({
    url: 'api/inbound/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
