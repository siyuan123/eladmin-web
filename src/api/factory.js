import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/factory',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/factory/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/factory',
    method: 'put',
    data
  })
}

export function downloadFactory(params) {
  return request({
    url: 'api/factory/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
