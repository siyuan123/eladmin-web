import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/hospital',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/hospital/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/hospital',
    method: 'put',
    data
  })
}

export function downloadHospital(params) {
  return request({
    url: 'api/hospital/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
