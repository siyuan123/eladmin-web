import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/outbound',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/outbound/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/outbound',
    method: 'put',
    data
  })
}

export function downloadOutbound(params) {
  return request({
    url: 'api/outbound/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
