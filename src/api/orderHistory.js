import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/orderHistory',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/orderHistory/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/orderHistory',
    method: 'put',
    data
  })
}

export function downloadOrderHistory(params) {
  return request({
    url: 'api/orderHistory/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
