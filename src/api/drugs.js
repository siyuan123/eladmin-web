import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/drugs',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/drugs/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/drugs',
    method: 'put',
    data
  })
}

export function downloadDrugs(params) {
  return request({
    url: 'api/drugs/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
